-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 06, 2018 at 03:54 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eventdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendees`
--

CREATE TABLE `attendees` (
  `attendees_id` smallint(5) UNSIGNED NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `contact_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendees`
--

INSERT INTO `attendees` (`attendees_id`, `firstname`, `lastname`, `contact_number`) VALUES
(50210, 'Jomar', 'Edano', 2147483647),
(50211, 'Angelie', 'Escaran', 2147483647),
(50212, 'Arona', 'Cartilla', 2147483647);

-- --------------------------------------------------------

--
-- Table structure for table `attendeetype`
--

CREATE TABLE `attendeetype` (
  `attendeeType_id` smallint(5) UNSIGNED NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendeetype`
--

INSERT INTO `attendeetype` (`attendeeType_id`, `status`) VALUES
(1, 'General Public'),
(2, 'Students');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `event_id` smallint(5) UNSIGNED NOT NULL,
  `organizer_id` smallint(5) UNSIGNED NOT NULL,
  `attendees_id` smallint(5) UNSIGNED NOT NULL,
  `attendeeType_id` smallint(5) UNSIGNED NOT NULL,
  `event_name` varchar(50) NOT NULL,
  `event_date` date NOT NULL,
  `event_time` time NOT NULL,
  `location` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `organizer_id`, `attendees_id`, `attendeeType_id`, `event_name`, `event_date`, `event_time`, `location`) VALUES
(1, 1001, 50211, 2, 'Talks at PN', '2018-03-10', '09:00:00', 'Evangeline'),
(2, 1001, 50210, 2, 'Talks at PN', '2018-03-10', '09:00:00', 'Evangeline');

-- --------------------------------------------------------

--
-- Table structure for table `organizers`
--

CREATE TABLE `organizers` (
  `organizer_id` smallint(5) UNSIGNED NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organizers`
--

INSERT INTO `organizers` (`organizer_id`, `firstname`, `lastname`) VALUES
(1001, 'Roy', 'Gallano'),
(1002, 'Riza', 'Barazon');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendees`
--
ALTER TABLE `attendees`
  ADD PRIMARY KEY (`attendees_id`);

--
-- Indexes for table `attendeetype`
--
ALTER TABLE `attendeetype`
  ADD PRIMARY KEY (`attendeeType_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`),
  ADD KEY `FK_organizerID` (`organizer_id`),
  ADD KEY `FK_attendeesID` (`attendees_id`),
  ADD KEY `FK_attendeeTypeID` (`attendeeType_id`);

--
-- Indexes for table `organizers`
--
ALTER TABLE `organizers`
  ADD PRIMARY KEY (`organizer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendees`
--
ALTER TABLE `attendees`
  MODIFY `attendees_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50214;
--
-- AUTO_INCREMENT for table `attendeetype`
--
ALTER TABLE `attendeetype`
  MODIFY `attendeeType_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `event_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `organizers`
--
ALTER TABLE `organizers`
  MODIFY `organizer_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1004;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `FK_attendeeTypeID` FOREIGN KEY (`attendeeType_id`) REFERENCES `attendeetype` (`attendeeType_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_attendeesID` FOREIGN KEY (`attendees_id`) REFERENCES `attendees` (`attendees_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_organizerID` FOREIGN KEY (`organizer_id`) REFERENCES `organizers` (`organizer_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
